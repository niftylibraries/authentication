<?php

    /**
     * Interface for authentication storage adapters
     *
     * @category   Nifty
     * @package    Authentication
     * @author     Piers Simms <piers@c1h.co.uk>
     * @copyright  2014 Piers Simms
     * @version    0.2.4 Build 06041
     * @since      0.1
     */

    namespace Nifty\Authentication\Storage\Adapter;

    use Nifty\Storage\Adapter\StorageAdapterException;

    /**
     * AuthenticationStorageAdapterInterface interface.
     */
    interface AuthenticationStorageAdapterInterface {

        /**
         * Return the credential associated with an identity.
         *
         * @access public
         * @param mixed $identity
         * @return void
         */
        public function findCredentialByIdentity($identity);

    }

    /**
     * AuthenticationStorageAdapterException class.
     *
     * @extends StorageAdapterException
     */
    class AuthenticationStorageAdapterException extends StorageAdapterException {}