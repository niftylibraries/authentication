<?php

    /**
     * Base class for autentication method collections
     *
     * @category   Nifty
     * @package    Authentication
     * @author     Piers Simms <piers@c1h.co.uk>
     * @copyright  2014 Piers Simms
     * @version    0.2.4 Build 06041
     * @since      0.2.2
     */

    namespace Nifty\Authentication\Collections;

    /**
     * Abstract AbstractAuthenticationMethodCollection class.
     *
     * @abstract
     */
    abstract class AbstractAuthenticationMethodCollection {

        /**
         * Array of authentication methods
         *
         * (default value: array())
         *
         * @var array
         * @access protected
         */
        protected $methods = array();

        /**
         * Returns all authentication methods as an array.
         *
         * @access public
         * @return void
         */
        public function getMethods(){
            return $this->methods;
        }

    }

    /**
     * AuthenticationMethodCollectionException class.
     */
    class AuthenticationMethodCollectionException extends \Exception {}