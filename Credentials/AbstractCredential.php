<?php

    /**
     * Credential base class
     *
     * Used to check if an object is an instance of a credential, also offer base methods
     *
     * @category   Nifty
     * @package    Authentication
     * @author     Piers Simms <piers@c1h.co.uk>
     * @copyright  2014 Piers Simms
     * @version    0.2.4 Build 06041
     * @since      0.1
     */

    namespace Nifty\Authentication\Credentials;

    /**
     * Base Credential class
     *
     * @abstract
     */
    abstract class AbstractCredential {

        /**
         * Credential
         *
         * @var mixed
         * @access protected
         */
        protected $credential;

        /**
         * Returns the credential.
         *
         * @access public
         * @return mixed
         */
        public function getCredential(){
            return $this->credential;
        }

        /**
         * Sets the credential.
         *
         * @access public
         * @param mixed $credential
         * @return mixed
         */
        public function setCredential($credential){
            $this->credential = $credential;
            return $this;
        }

        /**
         * Checks if credential is not null.
         *
         * @access public
         * @return bool
         */
        public function hasCredential(){
            return !is_null($this->credential);
        }

        /**
         * Empties the credential.
         *
         * @access public
         * @return void
         */
        public function clearCredential(){
            $this->credential = null;
            return $this;
        }

    }

    /**
     * CredentialException class.
     */
    class CredentialException extends \Exception {}