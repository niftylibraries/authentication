<?php

    /**
     * Service to run authentication
     *
     * Runs authenticator and ties in all other components needed
     *
     * @category   Nifty
     * @package    Authentication
     * @author     Piers Simms <piers@c1h.co.uk>
     * @copyright  2014 Piers Simms
     * @version    0.2.4 Build 06041
     * @since      0.1
     */

    namespace Nifty\Authentication;

    use Nifty\Authentication\Collections\AbstractAuthenticationMethodCollection;
    use Nifty\Authentication\Methods\AbstractAuthenticationMethod;
    use Nifty\Storage\Access\Collections\StorageAccessCollectionInterface;

    /**
     * AuthenticationService class.
     */
    class AuthenticationService {

        /**
         * Whether the executed authentication method authenticated or not
         *
         * (default value: false)
         *
         * @var bool
         * @access protected
         */
        protected $authenticated = false;

        /**
         * Collection of authentication methods
         *
         * @var mixed
         * @access protected
         */
        protected $methods;

        /**
         * Chosen authentication method
         *
         * @var mixed
         * @access protected
         */
        protected $whichMethod;

        /**
         * Collection of storage access objects
         *
         * @var mixed
         * @access protected
         */
        protected $accessCollection;

        /**
         * Set method and storage access collections if given.
         *
         * @access public
         * @param mixed $methods (default: null)
         * @param mixed $accessCollection (default: null)
         * @return void
         */
        public function __construct($methods = null, $accessCollection = null){
            $this->setMethods($methods)->setAccess($accessCollection);
        }

        /**
         * Sets the authentication method collection.
         *
         * @access public
         * @param AbstractAuthenticationMethodCollection $methods
         * @return $this
         */
        public function setMethods(AbstractAuthenticationMethodCollection $methods){
            $this->methods = $methods;
            return $this;
        }

        /**
         * Sets the storage access collection.
         *
         * @access public
         * @param StorageAccessCollectionInterface $accessCollection
         * @return $this
         */
        public function setAccess(StorageAccessCollectionInterface $accessCollection){
            $this->accessCollection = $accessCollection;
            return $this;
        }

        /**
         * Returns the chosen authentication method.
         *
         * @access public
         * @return AbstractAuthenticationMethod
         */
        public function getWhichMethod(){
            return $this->whichMethod;
        }

        /**
         * Sets the chosen authentication method given an instance of an authentication method.
         *
         * @access public
         * @param AbstractAuthenticationMethod $whichMethod
         * @return $this
         */
        public function setWhichMethod(AbstractAuthenticationMethod $whichMethod){
            $this->whichMethod = $whichMethod;
            return $this;
        }

        /**
         * Determines which authentication method to execute based on a boolean test.
         *
         * @access public
         * @return mixed an authentication method if it finds one to use, or false on failure
         */
        public function whichMethod(){
            foreach($this->methods->getMethods() as $methodObj){ // run each method's test
                if($methodObj->assertShouldRun()){
                    $this->setWhichMethod($methodObj);
                    return $this->getWhichMethod();
                }
            }
            return false;
        }

        /**
         * Executes the chosen authentication method.
         *
         * @access public
         * @return bool
         */
        public function run(){
            $method = $this->whichMethod();
            if($method){ // if we've set the method in AuthenticationService::whichMethod()
                $method->setAccess($this->accessCollection);
                $this->authenticated = $method->run();
                return $this->isAuthenticated();
            } else {
                return false; // false if no method found
            }
        }

        /**
         * Checks if authentication service succeeded.
         *
         * @access public
         * @return bool
         */
        public function isAuthenticated(){
            return $this->authenticated;
        }

    }

    /**
     * AuthenticationServiceException class.
     */
    class AuthenticationServiceException extends \Exception {}