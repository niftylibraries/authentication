<?php

    /**
     * Base authentication method
     *
     * Magically store data, set a storage access collection and run update storage adapter hook
     *
     * @category   Nifty
     * @package    Authentication
     * @author     Piers Simms <piers@c1h.co.uk>
     * @copyright  2014 Piers Simms
     * @version    0.2.4 Build 06041
     * @since      0.2.2
     */

    namespace Nifty\Authentication\Methods;

    use Nifty\Storage\Access\Collections\StorageAccessCollectionInterface;

    /**
     * Abstract AbstractAuthenticationMethod class.
     *
     * @abstract
     */
    abstract class AbstractAuthenticationMethod {

        /**
         * Indiscriminate data
         *
         * (default value: array())
         *
         * @var array
         * @access protected
         */
        protected $data = array();

        /**
         * A storage access collection
         *
         * (default value: array())
         *
         * @var array
         * @access protected
         */
        protected $accessCollection = array();

        /**
         * Returns data from array.
         *
         * @access public
         * @param mixed $name
         * @return mixed the data if set or null
         */
        public function __get($name){
            return isset($this->data[$name]) ? $this->data[$name] : null;
        }

        /**
         * Sets data to array.
         *
         * @access public
         * @param mixed $name
         * @param mixed $data
         * @return void
         */
        public function __set($name, $data){
            $this->data[$name] = $data;
        }

        /**
         * Sets storage access collection and runs storage adapter hook.
         *
         * @access public
         * @param StorageAccessCollectionInterface $accessCollection
         * @return $this
         */
        public function setAccess(StorageAccessCollectionInterface $accessCollection){
            $this->accessCollection = $accessCollection;
            $this->updateStorageAdapter();
            return $this;
        }

        /**
         * Storage adapater hook callback.
         *
         * Empty by default but can be overwritten if necessary.
         *
         * @access protected
         * @return void
         */
        protected function updateStorageAdapter(){}

        /**
         * Tests whether this authentication method should be executed.
         *
         * @access public
         * @abstract
         * @return bool
         */
        abstract public function assertShouldRun();

        /**
         * Execute the authentication method's logic.
         *
         * @access public
         * @abstract
         * @return void
         */
        abstract public function run();

    }

    /**
     * AuthenticationMethodException class.
     */
    class AuthenticationMethodException extends \Exception {}