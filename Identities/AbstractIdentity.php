<?php

    /**
     * Identity base class
     *
     * Used to check if an object is an instance of a identity, also offer base methods
     *
     * @category   Nifty
     * @package    Authentication
     * @author     Piers Simms <piers@c1h.co.uk>
     * @copyright  2014 Piers Simms
     * @version    0.2.4 Build 06041
     * @since      0.1
     */

    namespace Nifty\Authentication\Identities;

    /**
     * Base Identity class
     *
     * @abstract
     */
    abstract class AbstractIdentity {

        /**
         * Identity
         *
         * @var mixed
         * @access protected
         */
        protected $identity;

        /**
         * Returns the identity.
         *
         * @access public
         * @return mixed
         */
        public function getIdentity(){
            return $this->identity;
        }

        /**
         * Sets the identity.
         *
         * @access public
         * @param mixed $identity
         * @return mixed
         */
        public function setIdentity($identity){
            $this->identity = $identity;
            return $this;
        }

        /**
         * Checks if identity is not null.
         *
         * @access public
         * @return bool
         */
        public function hasIdentity(){
            return !is_null($this->identity);
        }

        /**
         * Empties the identity.
         *
         * @access public
         * @return void
         */
        public function clearIdentity(){
            $this->identity = null;
            return $this;
        }

    }

    /**
     * IdentityException class.
     */
    class IdentityException extends \Exception {}